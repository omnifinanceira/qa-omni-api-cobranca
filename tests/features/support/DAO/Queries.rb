class Queries < SQLConnect

    require_relative 'SQLConnect'

    def busca_carteiras_assessoria()
        lista_carteiras_assessoria = select("SELECT co.contrato , CAST(CAST(co.cliente_contrato AS FLOAT) AS INTEGER) cliente, listagg(p.parcela) within group( order by p.parcela) parcelas
        FROM ca_arquivo_assessoria ar,
        ca_arquivo_assessoria_contrato co,
        contratos co2,
        operacoes ope,
        prestacoes p
        WHERE ope.codigo = co2.operacao
        AND co2.contrato = co.contrato
        AND ar.sequencia = co.sequencia_arquivo
        AND ar.tipo_arquivo = co.tipo_arquivo
        AND p.contrato = co.contrato
        AND ar.tipo_arquivo = 1
        AND ar.assessoria = 2931
        AND co.data_retirada is null
        AND p.liquidacao is null
        and p.data_ven < sysdate
        AND not exists (
        select * from acordo where contrato = co.contrato and status not in (1, 2, 8) and acordo.nr_seq_acordo =
        (select max(nr_seq_acordo) from acordo where contrato = co.contrato)
        )
        GROUP BY co.contrato, CAST(CAST(co.cliente_contrato AS FLOAT) AS INTEGER)")
        return lista_carteiras_assessoria
    end

    def recusa_acordos()
        require_relative '../../step_definitions/simula_acordo.rb'

        cliente = $dados_solicita_aprovacao[0]
        contrato = $dados_solicita_aprovacao[1]
        nr_seq_acordo = $dados_solicita_aprovacao[2]

        statement = "UPDATE ACORDO
        SET STATUS = 2,
        DT_QUEBRA_ACORDO = SYSDATE
        WHERE CONTRATO = :contrato
        AND COD_CLIENTE = :cliente
        AND NR_SEQ_ACORDO = :nr_seq_acordo"

        $cursor = $connectionstring.parse(statement)
        $cursor.bind_param(:contrato,"#{contrato}")
        $cursor.bind_param(:cliente, "#{cliente}")
        $cursor.bind_param(:nr_seq_acordo, "#{nr_seq_acordo}")
        $cursor.exec
        $connectionstring.commit
    end

end