require_relative '../pages/request.rb'
require_relative '../pages/util.rb'
require 'allure-cucumber'
require 'faker'
require 'httparty'
require "json-schema"
require 'jsonpath'
require 'pry'
require 'oci8'
require 'yaml'
require 'net/http'
require 'uri'
require 'nokogiri'

AMBIENTE = ENV['AMBIENTE']
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")
BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

util = Util.new

    AllureCucumber.configure do |config|
        config.results_directory = "/reports"
        config.clean_results_directory = true
        config.logging_level = Logger::INFO
        config.logger = Logger.new($stdout, Logger::DEBUG)
        config.environment = "staging"
        config.environment_properties = {
        custom_attribute: "API WS_COB"
    }

        config.categories = File.new("categories/my_custom_categories.json")
    end