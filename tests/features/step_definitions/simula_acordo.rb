Dado('que eu faça a requisição para buscar o saldo atualizado de um contrato') do
    @saldo = SaldoAtualizado.new
    @util = Util.new
    
    $classe_saldo_atualizado_soap = SaldoAtualizadoSoap.new
    $simula_proposta_soap = SimulaPropostaSoap.new
    $simula_proposta = SimulaProposta.new
    $solicita_aprovacao = SolicitaAprovacao.new
    $solicita_url_boleto = UrlBoleto.new
    $status_aprovacao = StatusAprovacao.new
    $status_aprovacao_soap = StatusAprovacaoSoap.new
    carteiras =  @util.pega_carteiras_assessoria
    $contrato = carteiras[0][0]
    $cliente = carteiras[0][1]
    $parcelas = carteiras[0][2]
    @saldo_atualizado = @saldo.busca_saldo($token, $contrato, $cliente)
    $saldo_atualizado_postman = @saldo_atualizado['saldoAtualizado']['valorDivida']
end

Então('devo verificar se a requisição da atualização da dívida foi executada com sucesso') do
    expect(@saldo_atualizado.code).to eq(200)
end

Então('vou verificar se o valor do saldo atualizado é semelhante ao encontrado no Soapui') do
    $saldo_atualizado_soap = $classe_saldo_atualizado_soap.exec_post_saldo_atualizado_soap($contrato, $cliente)
    saldo_atualizado_soap_convertido = $saldo_atualizado_soap.to_f
    expect($saldo_atualizado_postman).to eq(saldo_atualizado_soap_convertido)
end

Dado('que eu faça a requisição para buscar as parcelas vencidas de um contrato') do
    @util = Util.new
   
    carteiras =  @util.pega_carteiras_assessoria
    $contrato = carteiras[0][0]
    $cliente = carteiras[0][1]
    $data_corrente = @util.gera_data_hoje
    

    @controle_parcelas = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcelas.busca_parcelas_vencidas($contrato,$token,$data_corrente)
   
    array_size_response = @parcelas_vencidas_post['parcelasVencidas'].count
    $response_rest_parcelas_vencidas = []
    control = 0
    while control < array_size_response
    
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['numero']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['valor']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['vencimento']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['principal']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['multa']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['mora']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['jurosRemuneratorios']
        $response_rest_parcelas_vencidas << @parcelas_vencidas_post['parcelasVencidas'][control]['despesas']
        control+=1
    end
   
end

Então('devo verificar se a requisição de parcela vencida foi executada com sucesso') do
    expect(@parcelas_vencidas_post.code).to eq(200)
    expect(@parcelas_vencidas_post['status']).to eq("SUCCESS")
end

Então('vou verificar se o valor da dívida é semelhante ao encontrado no Soapui') do
    @parcela_vencida_SOAP = ParcelasVencidasSoap.new
    $parcela_vencida_values_SOAP = @parcela_vencida_SOAP.exec_post_parcelas_vencidas_soap($contrato,$cliente,$data_atual) 

    control =0
    while control < $parcela_vencida_values_SOAP.count
    expect($parcela_vencida_values_SOAP[control]).to eq($response_rest_parcelas_vencidas[control])
    
    control+=1
    end
   
   
end

Dado('que eu faça a requisição enviando o campo dataPagamento vazio') do
    @util = Util.new
    carteiras =  @util.pega_carteiras_assessoria
    $contrato = carteiras[0][0]
    $cliente = carteiras[0][1]
    $data_corrente = ""

    @controle_parcelas_dataPagamento_vazio = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcelas_dataPagamento_vazio.busca_parcelas_vencidas($contrato,$token,$data_corrente)
    

  end
  
  Então('devo verificar se o erro {string} foi obtido como resposta com a mensagem {string}') do |statusCode, returnedMessage|
    
    messageResponse = @parcelas_vencidas_post['messages'].to_s.gsub(/"|\[|\]/,"")
    expect(@parcelas_vencidas_post.code).to eq(statusCode.to_i)
    expect(messageResponse).to eq(returnedMessage)

  end

  Então('vou verificar se no Soapui o status obtido foi {string} com a mensagem {string}') do |statusCode, message|
   
    $data_atual= "0"
    @parcela_vencida_SOAP = ParcelasVencidasSoap.new
    $parcela_vencida_values_SOAP = @parcela_vencida_SOAP.exec_post_parcelas_vencidas_soap_negativo($contrato,$cliente,$data_atual) 

    expect($parcela_vencida_values_SOAP[0]).to eq(statusCode)
    expect($parcela_vencida_values_SOAP[1]).to eq(message)

  end
  
  Dado('que eu faça a requisição enviando o campo contrato vazio') do
    @util = Util.new
    carteiras =  @util.pega_carteiras_assessoria
    $contrato = ""
    $cliente = carteiras[0][1]
    $data_corrente = @util.gera_data_hoje

    @controle_parcela_contrato_vazio = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcela_contrato_vazio.busca_parcelas_vencidas($contrato,$token,$data_corrente)
    
  end
  
  Então('devo verificar se o erro {string} foi obtido como resposta com a mensagem {string} contrato vazio') do |statusCode, message|
    messageResponse = @parcelas_vencidas_post['messages'].to_s.gsub(/"|\[|\]/,"")
    expect(@parcelas_vencidas_post.code).to eq(statusCode.to_i)
    expect(messageResponse).to eq(message)
  end
  
  Então('vou verificar se no Soapui o status obtido foi {string}') do |statusCodeSOAP|
    
    @parcela_vencida_SOAP = ParcelasVencidasSoap.new
    $parcela_vencida_values_SOAP = @parcela_vencida_SOAP.exec_post_parcelas_vencidas_soap_negativo($contrato,$cliente,$data_atual) 
  
    expect($parcela_vencida_values_SOAP[0]).to eq(statusCodeSOAP)
    
  end
  

  Dado('que eu faça a requisição enviando o campo dataPagamento zerado') do

    @util = Util.new
    carteiras =  @util.pega_carteiras_assessoria
    $contrato = carteiras[0][0]
    $cliente = carteiras[0][1]
    $data_corrente = "0"

    @controle_parcela_contrato_vazio = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcela_contrato_vazio.busca_parcelas_vencidas($contrato,$token,$data_corrente)
  end
  
  Então('devo verificar se o erro {string} foi obtido como resposta sem mensagem') do |statusCode|
    expect(@parcelas_vencidas_post.code).to eq(statusCode.to_i)
  end

  Então('vou verificar se no Soapui o status obtido foi {string} com a mensagem {string} dataPagamento zerado') do |statusCode, message|
    $data_atual= "0"
    @parcela_vencida_SOAP = ParcelasVencidasSoap.new
    $parcela_vencida_values_SOAP = @parcela_vencida_SOAP.exec_post_parcelas_vencidas_soap_negativo($contrato,$cliente,$data_atual) 
 
    expect($parcela_vencida_values_SOAP[0]).to eq(statusCode)
    expect($parcela_vencida_values_SOAP[1]).to eq(message)
  end
  

  Dado('que eu faça a requisição enviando o campo contrato zerado') do
    @util = Util.new
    carteiras =  @util.pega_carteiras_assessoria
    contrato = "0"
    $cliente = carteiras[0][1]
    $data_corrente = @util.gera_data_hoje

    @controle_parcela_contrato_vazio = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcela_contrato_vazio.busca_parcelas_vencidas(contrato,$token,$data_corrente)

  end
  
  Então('devo verificar se o status obtido foi {string}') do |statusCode|
    expect(@parcelas_vencidas_post.code).to eq(statusCode.to_i)
  end


  Dado('que eu faça a requisição enviando uma string no campo dataPagamento') do
    @util = Util.new
    carteiras =  @util.pega_carteiras_assessoria
    $cliente = carteiras[0][1]
    $data_corrente = "testeData"

    @controle_parcela_contrato_vazio = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcela_contrato_vazio.busca_parcelas_vencidas($contrato,$token,$data_corrente)
  end
  
  Então('devo verificar se o erro {int} foi obtido como resposta sem mensagem') do |int|
    expect(@parcelas_vencidas_post.code).to eq(statusCode.to_i)
  end
  
  Então('vou verificar se no Soapui o status obtido foi {string} com a mensagem {string}  para o campo de dataPagamento') do |statusCode, message|
    $data_atual= "testeDataSOAP"
    @parcela_vencida_SOAP = ParcelasVencidasSoap.new
    $parcela_vencida_values_SOAP = @parcela_vencida_SOAP.exec_post_parcelas_vencidas_soap_negativo($contrato,$cliente,$data_atual) 
  
    expect($parcela_vencida_values_SOAP[0]).to eq(statusCode)
    expect($parcela_vencida_values_SOAP[1]).to eq(message)
  end
  
  Dado('que eu faça a requisição enviando uma string no campo contrato') do
    @util = Util.new
    carteiras =  @util.pega_carteiras_assessoria
    $contrato = "testeQA_automacao"
    $cliente = carteiras[0][1]
    $data_corrente = @util.gera_data_hoje

    @controle_parcela_contrato_vazio = ParcelasVencidas.new
    @parcelas_vencidas_post = @controle_parcela_contrato_vazio.busca_parcelas_vencidas($contrato,$token,$data_corrente)
  end

  

Dado('que eu faça a requisição para simulação de proposta à vista com o mesmo valor da dívida') do
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end

Então('devo verificar se a requisição de geração de acordo foi executada com sucesso') do
    expect($acordo_postman.code).to eq(200)
end

Quando('eu realizar a requisição para simulação de proposta à vista com o mesmo valor da dívida no Soapui') do
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_soap = $simula_proposta_soap.exec_post_simula_proposta_soap($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)
end

Então('vou verificar se o valor total da dívida são semelhantes aos do Soapui') do
    total_divida_postman = $acordo_postman['simulaAcordo']['composicaoAcordo']['totalDivida']
    total_divida_soap = $acordo_soap[3]
    total_divida_soap_convertido = total_divida_soap.to_f
    expect(total_divida_postman).to eq(total_divida_soap_convertido)
end

Então('devo verificar se os valores máximos e mínimos dos honorários são semelhantes aos do Soapui') do
    honorario_minimo_postman = $acordo_postman['simulaAcordo']['pagamento']['entrada']['valorHonorario']
    honorario_max_postman = $acordo_postman['simulaAcordo']['pagamento']['entrada']['valorMaxHonorario']
    honorario_minimo_soap = $acordo_soap[1]
    honorario_minimo_soap_convertido = honorario_minimo_soap.to_f
    honorario_max_soap = $acordo_soap[2]
    honorario_max_soap_convertido = honorario_max_soap.to_f
    expect(honorario_minimo_postman).to eq(honorario_minimo_soap_convertido)
    expect(honorario_max_postman).to eq(honorario_max_soap_convertido)
end

Dado('que eu faça a requisição para aprovação da proposta à vista') do
    forma_pagamento = "1"
    parcela_valor = "0"
    
    $dados_solicita_aprovacao = []
    $dados_solicita_aprovacao << $cliente 
    $dados_solicita_aprovacao << $contrato

    $solicita_aprovacao_postman = $solicita_aprovacao.solicita_aprovacao($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
    nr_seq_acordo = $solicita_aprovacao_postman['acordoResultado']['numeroSequencia']

    $dados_solicita_aprovacao << nr_seq_acordo
end

Então('devo verificar se a requisição de solicitação de aprovação foi executada com sucesso') do
    expect($solicita_aprovacao_postman.code).to eq(200)
end

Dado('que eu faça a requisição para solicitar a URL do boleto da proposta à vista') do
    @url = $solicita_url_boleto.solicita_url_boleto($token, $contrato, $cliente)
end

Então('devo verificar se a requisição do boleto foi executada com sucesso') do
    expect(@url.code).to eq(200)
end

Dado('que eu faça a requisição para simulação de proposta a prazo com o valor dez por cento menor que a dívida') do 
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.9).to_f
    parcela_valor = saldo_com_desconto/2
    $acordo_postman = $simula_proposta.simula_proposta($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_com_desconto, $token)
end

Quando('eu realizar a requisição para simulação de proposta a prazo com o valor dez por cento menor que a dívida no Soapui') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.9).to_f
    parcela_valor = saldo_com_desconto/2
    $acordo_soap = $simula_proposta_soap.exec_post_simula_proposta_soap($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_com_desconto)
end

Então('vou verificar se os valores máximos e mínimos por parcela são semelhantes aos do Soapui') do
    parcelas_max_postman = $acordo_postman['simulaAcordo']['pagamento']['parcela']['qtdeMaximaParcelas']
    parcelas_max_soap = $acordo_soap[5]
    parcelas_max_soap_convertido = parcelas_max_soap.to_f
    expect(parcelas_max_postman).to eq(parcelas_max_soap_convertido)
    parcelas_min_postman = $acordo_postman['simulaAcordo']['pagamento']['parcela']['qtdeParcelas']
    parcelas_min_soap = $acordo_soap[4]
    parcelas_min_soap_convertido = parcelas_min_soap.to_f
    expect(parcelas_min_postman).to eq(parcelas_min_soap_convertido)
end

Então('verificar se os valores máximos e mínimos de entrada são semelhantes aos do Soapui') do
    valor_minimo_postman = $acordo_postman['simulaAcordo']['pagamento']['entrada']['valorMinimo']
    valor_minimo_soap = $acordo_soap[0]
    valor_minimo_soap_convertido = valor_minimo_soap.to_f
    expect(valor_minimo_postman).to eq(valor_minimo_soap_convertido)
end

Dado('que eu faça a requisição para aprovação da proposta a prazo com o valor dez por cento menor que a dívida') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.9).to_f
    parcela_valor = saldo_com_desconto/2

    $dados_solicita_aprovacao = []
    $dados_solicita_aprovacao << $cliente 
    $dados_solicita_aprovacao << $contrato

    $solicita_aprovacao_postman = $solicita_aprovacao.solicita_aprovacao($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
    nr_seq_acordo = $solicita_aprovacao_postman['acordoResultado']['numeroSequencia']

    $dados_solicita_aprovacao << nr_seq_acordo
end

Dado('que eu faça a requisição para solicitação do status da proposta') do
    @status = $status_aprovacao.status_aprovacao($token, $contrato, $cliente)
    @status_postman = @status['statusAcordo']['historico']
end

Então('devo verificar se a requisição de verificação de status foi executada com sucesso') do
    expect(@status.code).to eq(200)
end

Então('vou verificar se está com o status aprovado') do
    expect(@status_postman.include?('PROPOSTA APROVADA')).to be true
end

Então('vou verificar se o status da proposta é semelhante ao encontrado no Soapui') do
    status_soap = $status_aprovacao_soap.exec_post_status_aprovacao_soap($contrato, $cliente)
    expect(status_soap.include?('PROPOSTA APROVADA')).to be true
end

Dado('que eu faça a requisição para simulação de proposta a prazo com o valor vinte por cento menor que a dívida') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.8).to_f
    parcela_valor = saldo_com_desconto/2
    $acordo_postman = $simula_proposta.simula_proposta($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_com_desconto, $token)
end

Quando('eu realizar a requisição para simulação de proposta a prazo com o valor vinte por cento menor que a dívida no Soapui') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.8).to_f
    parcela_valor = saldo_com_desconto/2
    $acordo_soap = $simula_proposta_soap.exec_post_simula_proposta_soap($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_com_desconto)
end

Dado('que eu faça a requisição para aprovação da proposta a prazo com o valor vinte por cento menor que a dívida') do 
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.8).to_f
    parcela_valor = saldo_com_desconto/2

    $dados_solicita_aprovacao = []
    $dados_solicita_aprovacao << $cliente 
    $dados_solicita_aprovacao << $contrato

    $solicita_aprovacao_postman = $solicita_aprovacao.solicita_aprovacao($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
    nr_seq_acordo = $solicita_aprovacao_postman['acordoResultado']['numeroSequencia']

    $dados_solicita_aprovacao << nr_seq_acordo
end

Dado('que eu faça a requisição para simulação de proposta a prazo com o valor cinquenta por cento menor que a dívida') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.5).to_f
    parcela_valor = saldo_com_desconto/2
    $acordo_postman = $simula_proposta.simula_proposta($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_com_desconto, $token)
end

Quando('eu realizar a requisição para simulação de proposta a prazo com o valor cinquenta por cento menor que a dívida no Soapui') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.5).to_f
    parcela_valor = saldo_com_desconto/2
    $acordo_soap = $simula_proposta_soap.exec_post_simula_proposta_soap($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_com_desconto)
end

Dado('que eu faça a requisição para aprovação da proposta a prazo com o valor cinquenta por cento menor que a dívida') do
    forma_pagamento = "2"
    saldo_com_desconto = ($saldo_atualizado_postman*0.5).to_f
    parcela_valor = saldo_com_desconto/2

    $dados_solicita_aprovacao = []
    $dados_solicita_aprovacao << $cliente 
    $dados_solicita_aprovacao << $contrato

    $solicita_aprovacao_postman = $solicita_aprovacao.solicita_aprovacao($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
    nr_seq_acordo = $solicita_aprovacao_postman['acordoResultado']['numeroSequencia']

    $dados_solicita_aprovacao << nr_seq_acordo
end

Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida sem informar o nome Id do Cliente') do
    cliente = ""
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta(cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end

  Então('eu verifico que o erro {string} foi obtido como resposta e a mensagem {string}') do |statusCode, returnedMessage|
    messageResponse = $acordo_postman['messages'].to_s.gsub(/"|\[|\]/,"")
    expect($acordo_postman.code).to eq(statusCode.to_i)
    expect(messageResponse).to eq(returnedMessage)
end

Então('devo verificar se o Status code obtido foi {string}') do |statusCode|
  expect($acordo_postman.code).to eq(statusCode.to_i)
end

Então('eu verifico que o erro {string} foi obtido como resposta e o código {string}') do |statusCode, returnedMessage|
    messageResponse = $acordo_postman['messages'].to_s
    expect($acordo_postman.code).to eq(statusCode.to_i)
    expect(messageResponse).to eq(returnedMessage)
  end

  Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando o Id do Cliente incorreto') do
    cliente = "9999999"
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta(cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end
  
  Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida sem informar o valor da entrada') do
    forma_pagamento = "1"
    parcela_valor = "0"
    saldo_atualizado_postman = ""
    $acordo_postman = $simula_proposta.simula_proposta($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_atualizado_postman, $token)
end
    
  Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida sem informar a data') do
    data_simulacao = ""
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta_data($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token, data_simulacao)
end
  
  Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando o codigo incorreto de parcelamento') do
    forma_pagamento = "3"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end

  Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando um numero invalido de contrato') do
    contrato = "999999999999999"
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta($cliente, contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end
  
  Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando um numero em branco de contrato') do
    contrato = ""
    forma_pagamento = "1"
    parcela_valor = "0"
    $acordo_postman = $simula_proposta.simula_proposta($cliente, contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end

Então('verifico se no Soapui o status obtido foi {string} com a mensagem {string} cliente vazio') do |statusCode, message|
  cliente = ""
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo(cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Então('verifico se no Soapui o status obtido foi {string} com a mensagem {string} cliente incorreto') do |statusCode, message|
  cliente = "999999999"
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo(cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Então('verifico se no Soapui o status obtido foi {string} com a mensagem {string} entrada vazia') do |statusCode, message|
  saldo_atualizado_soap = ""
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Então('verifico se no Soapui o status obtido foi {string} com a mensagem {string} Cod Parcelamento Incorreto') do |statusCode, message|
  forma_pagamento = "3"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Então('verifico se no Soapui o status obtido foi {string} com a mensagem {string} contrato vazio') do |statusCode, message|
  contrato = ""
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo($cliente, contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Dado('verifico se no Soapui o status obtido foi {string} com a mensagem {string} data vazia') do |statusCode, message|
  data_simulacao_soap = ""
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_data($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap, data_simulacao_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida string no campo data') do
  data_simulacao = "TesteData"
  forma_pagamento = "1"
  parcela_valor = "0"
  $acordo_postman = $simula_proposta.simula_proposta_data($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token, data_simulacao)
end

Dado('que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando uma string no numero de contrato') do
  contrato = "TesteContrato"
  forma_pagamento = "1"
  parcela_valor = "0"
  $acordo_postman = $simula_proposta.simula_proposta($cliente, contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_postman, $token)
end

Então('verifico se no Soapui o status obtido foi {string} com a mensagem {string} com string no campo data') do |statusCode, message|
  data_simulacao_soap = "TesteData"
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_data($cliente, $contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap, data_simulacao_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
  expect($simula_acorodo_values_SOAP[1]).to eq(message)
end

Então('eu verifico que o erro {string} foi obtido como resposta') do |statusCode|
  expect($acordo_postman.code).to eq(statusCode.to_i)
end

Então('verifico se no Soapui o status obtido foi {string} com o numero de contrato incorreto') do |statusCode|
  contrato = "999999999999999"
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo($cliente, contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
end

Então('verifico se no Soapui o status obtido foi {string} com uma string no numero de contrato') do |statusCode|
  contrato = "TesteContrato"
  forma_pagamento = "1"
  parcela_valor = "0"
  $simula_proposta_soap = SimulaPropostaSoap.new
  $simula_acorodo_values_SOAP = $simula_proposta_soap.exec_post_simula_proposta_soap_negativo($cliente, contrato, $parcelas, forma_pagamento, parcela_valor, $saldo_atualizado_soap)

  expect($simula_acorodo_values_SOAP[0]).to eq(statusCode)
end