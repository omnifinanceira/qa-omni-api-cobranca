Quando('eu preencher o campo de usuário {string}') do |usuario|
    @aut = Autenticacao.new
    @soap = AutenticacaoSoap.new
    @usuario = usuario
end

Quando('o campo de senha {string}') do |senha|
    @senha = senha
end

Entao('fazer a requisição') do
    @response_login = @aut.fazer_autenticacao(@usuario, @senha)
    $token =  @aut.retorna_token
    @soap.exec_post_aut_soap
end

Entao('devo validar o retorno do servico de autenticação') do
    expect(@response_login.code).to eq(200)
end