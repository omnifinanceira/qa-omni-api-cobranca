Dado('que eu faça a requisição para calcular uma possível operação com base em uma data para um futuro pagamento') do
    @util = Util.new
    @calcula_iof = CalculaIof.new
    @calcula_iof_soap = CalculaIofSoap.new
    carteiras =  @util.pega_carteiras_assessoria
    @contrato = carteiras[0][0]
    @parcelas = carteiras[0][2]
    @calculo = @calcula_iof.calcula_iof($token, @contrato, @parcelas)
    @calculo_iof_postman = @calculo['retornaIof']['valor']
end

Então('devo verificar se a requisição foi executada com sucesso') do
    expect(@calculo.code).to eq(200)
end

Então('vou verificar se o valor do cálculo é semelhante ao encontrado no Soapui') do
    calculo_iof_soap = @calcula_iof_soap.exec_post_calcula_iof_soap(@contrato, @parcelas)
    calculo_iof_soap_convertido = calculo_iof_soap.to_f
    expect(@calculo_iof_postman).to eq(calculo_iof_soap_convertido)
end