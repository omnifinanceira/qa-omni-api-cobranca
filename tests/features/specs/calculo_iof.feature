#language: pt

Funcionalidade: Calcular IOF de possíveis operações pela API ws_cob
- Para que eu possa acessar o sistema sendo um usuário/assessoria
- Para que eu possa calcular o IOF de possíveis operações
- E possa validar esses cálculos

Contexto: Pré-requisitos para autenticação no Omni Fácil
    * eu preencher o campo de usuário "2931MAURICIO"
    * o campo de senha "senha123"
    * fazer a requisição

@calculo_iof
Cenário: Realizando cálculo de IOF
    Dado que eu faça a requisição para calcular uma possível operação com base em uma data para um futuro pagamento
    Então devo verificar se a requisição foi executada com sucesso
    E vou verificar se o valor do cálculo é semelhante ao encontrado no Soapui