#language: pt

@login_api
Funcionalidade: Login no Omni Fácil
- Para que eu possa fazer a autenticação da API sendo um usuário

Cenário: Realizar login com sucesso
    Quando eu preencher o campo de usuário "2931MAURICIO"
    E o campo de senha "senha123"
    E fazer a requisição
    Então devo validar o retorno do servico de autenticação