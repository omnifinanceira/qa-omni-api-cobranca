#language: pt

Funcionalidade: Simular propostas pela API ws_cob
- Para que eu possa acessar o sistema sendo um usuário/assessoria
- Para que eu possa buscar o salto atualizado da dívida
- Para que eu possa buscar parcelas vencidas
- Para que eu possa gerar acordos à vista, parcelado e com desconto por contrato
- E possa validar os saldos atualizados das dívidas
- E possa validar as parcelas vencidas
- E possa validar os acordos gerados
- E possa solicitar aprovação dos acordos gerados
- Para que eu possa solicitar a URL do boleto
- E possa verificar o status dos acordos gerados

Contexto: Pré-requisitos para autenticação no Omni Fácil
    * eu preencher o campo de usuário "2931MAURICIO"
    * o campo de senha "senha123"
    * fazer a requisição

@busca_saldo_atualizado
Cenário: Buscando saldo atualizado da dívida
    Dado que eu faça a requisição para buscar o saldo atualizado de um contrato
    Então devo verificar se a requisição da atualização da dívida foi executada com sucesso
    E vou verificar se o valor do saldo atualizado é semelhante ao encontrado no Soapui

@parcelas_vencidas_campos_vazios_01
Cenário: Validando o serviço de parcelas vencidas com campo data vazio
	Dado que eu faça a requisição enviando o campo dataPagamento vazio 
	Então devo verificar se o erro '400' foi obtido como resposta com a mensagem "dataPagamento: É obrigatório informar a data de pagamento."
	E vou verificar se no Soapui o status obtido foi '-1847' com a mensagem "ORA-01847: day of month must be between 1 and last day of month"
	
@parcelas_vencidas_campos_vazios_02
Cenário: Validando o serviço de parcelas vencidas com campo contrato vazio
	Dado que eu faça a requisição enviando o campo contrato vazio
	Então devo verificar se o erro '400' foi obtido como resposta com a mensagem "ContratoNotFoundException" contrato vazio
	E vou verificar se no Soapui o status obtido foi '-10001'

@parcelas_vencidas_campos_zerados_01
Cenário: Validando o serviço de parcelas vencidas com campo data zerado
	Dado que eu faça a requisição enviando o campo dataPagamento zerado
	Então devo verificar se o erro '400' foi obtido como resposta sem mensagem
	E vou verificar se no Soapui o status obtido foi '-1847' com a mensagem "ORA-01847: day of month must be between 1 and last day of month" dataPagamento zerado

@parcelas_vencidas_campos_zerados_02
Cenário: Validando o serviço de parcelas vencidas com campo contrato zerado
	Dado que eu faça a requisição enviando o campo contrato zerado
	Então devo verificar se o status obtido foi '200'

@parcelas_vencidas_validacao_string_01
Cenário: Validando o serviço de parcelas vencidas enviando uma string no campo data 
	Dado que eu faça a requisição enviando uma string no campo dataPagamento 
	Então devo verificar se o erro '400' foi obtido como resposta sem mensagem
	E vou verificar se no Soapui o status obtido foi '-1858' com a mensagem "ORA-01858: a non-numeric character was found where a numeric was expected"  para o campo de dataPagamento
	
@simulação_acordo_campos_vazios_01
Cenario: Gerando o acordo sem informar o Id do Cliente 
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida sem informar o nome Id do Cliente 
    Então eu verifico que o erro '400' foi obtido como resposta e a mensagem "cliente: É obrigatório informar o código do cliente"
    E verifico se no Soapui o status obtido foi '-10001' com a mensagem 'C;digo do cliente nulo.' cliente vazio

@simulação_acordo_campos_incorretos_01
Cenario: Gerando o acordo informando o Id do Cliente incorreto  
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando o Id do Cliente incorreto  
    Então eu verifico que o erro '400' foi obtido como resposta
    E verifico se no Soapui o status obtido foi '7' com a mensagem 'CHAVE CONTRATO / CLIENTE INEXISTENTE' cliente incorreto 

@simulação_acordo_campos_vazios_02    
Cenario: Gerando o acordo com o valor da entrada vazia
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida sem informar o valor da entrada 
    Então eu verifico que o erro '400' foi obtido como resposta e a mensagem "pagamento.entrada.valor: É obrigatório informar o valor de entrada"
    E verifico se no Soapui o status obtido foi '-10002' com a mensagem 'Valor de entrada nula.' entrada vazia 

@simulação_acordo_campos_vazios_03
Cenario: Gerando o acordo com a data vazia
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida sem informar a data
    Então eu verifico que o erro '400' foi obtido como resposta e a mensagem "pagamento.dataPrimeiroPagamento: É obrigatório informar a data de pagamento"
    E verifico se no Soapui o status obtido foi '-10002' com a mensagem 'Data de pagamento nula.' data vazia 

@simulação_acordo_campos_string_01
Cenario: Gerando o acordo enviando uma string no campo data
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida string no campo data
    Então eu verifico que o erro '400' foi obtido como resposta e a mensagem ""
    E verifico se no Soapui o status obtido foi '-1858' com a mensagem 'ORA-01858: a non-numeric character was found where a numeric was expected' com string no campo data

@simulação_acordo_campos_incorretos_02
Cenario: Gerando o acordo informando codigo do parcelamento incorreto
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando o codigo incorreto de parcelamento 
    Então eu verifico que o erro '400' foi obtido como resposta
    E verifico se no Soapui o status obtido foi '9' com a mensagem 'FORMA DE PAGAMENTO MAIOR QUE 2' Cod Parcelamento Incorreto  

@simulação_acordo_campos_incorretos_03
Cenario: Gerando o acordo informando um numero de contrato incorreto
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando um numero invalido de contrato 
    Então eu verifico que o erro '400' foi obtido como resposta
    E verifico se no Soapui o status obtido foi '-20004' com o numero de contrato incorreto 

@simulação_acordo_campos_vazios_04
Cenario: Gerando o acordo informando um numero de contrato vazio
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando um numero em branco de contrato
    Então eu verifico que o erro '404' foi obtido como resposta e a mensagem "AcordoNotFoundException"
    E verifico se no Soapui o status obtido foi '-10001' com a mensagem 'N;mero do contrato nulo.' contrato vazio 

@simulação_acordo_campos_string_02
Cenario: Gerando o acordo informando uma string no numero de contrato
    Dado que eu faça a requisição para a simulação da proposta à vista com o mesmo valor da divida informando uma string no numero de contrato 
    Então eu verifico que o erro '400' foi obtido como resposta
    E verifico se no Soapui o status obtido foi '-20004' com uma string no numero de contrato

@busca_parcelas_vencidas
Cenário: Buscando parcelas vencidas do contrato
    Dado que eu faça a requisição para buscar as parcelas vencidas de um contrato
    Então devo verificar se a requisição de parcela vencida foi executada com sucesso
    E vou verificar se o valor da dívida é semelhante ao encontrado no Soapui

@gerando_acordo_api_assessoria_a_vista
Cenário: Gerando acordo no WS_COB com proposta à vista
    Dado que eu faça a requisição para simulação de proposta à vista com o mesmo valor da dívida
    Então devo verificar se a requisição de geração de acordo foi executada com sucesso
    Quando eu realizar a requisição para simulação de proposta à vista com o mesmo valor da dívida no Soapui
    Então vou verificar se o valor total da dívida são semelhantes aos do Soapui
    E verificar se os valores máximos e mínimos de entrada são semelhantes aos do Soapui
    E devo verificar se os valores máximos e mínimos dos honorários são semelhantes aos do Soapui

@solicita_aprovacao_proposta_a_vista
Cenário: Solicitando aprovação da proposta à vista
    Dado que eu faça a requisição para aprovação da proposta à vista
    Então devo verificar se a requisição de solicitação de aprovação foi executada com sucesso

@verifica_status_proposta_a_vista
Cenário: Verificando status da proposta à vista
    Dado que eu faça a requisição para solicitação do status da proposta
    Então devo verificar se a requisição de verificação de status foi executada com sucesso
    E vou verificar se está com o status aprovado
    Então vou verificar se o status da proposta é semelhante ao encontrado no Soapui

@executa_recusa_de_acordos
@solicita_url_boleto
Cenário: Solicitando url do boleto
    Dado que eu faça a requisição para solicitar a URL do boleto da proposta à vista
    Então devo verificar se a requisição do boleto foi executada com sucesso

@gerando_acordo_api_assessoria_a_prazo_01
Cenário: Gerando acordo no WS_COB com proposta a prazo
    Dado que eu faça a requisição para simulação de proposta a prazo com o valor dez por cento menor que a dívida
    Então devo verificar se a requisição de geração de acordo foi executada com sucesso
    Quando eu realizar a requisição para simulação de proposta a prazo com o valor dez por cento menor que a dívida no Soapui
    Então vou verificar se o valor total da dívida são semelhantes aos do Soapui
    E vou verificar se os valores máximos e mínimos por parcela são semelhantes aos do Soapui
    E verificar se os valores máximos e mínimos de entrada são semelhantes aos do Soapui
    E devo verificar se os valores máximos e mínimos dos honorários são semelhantes aos do Soapui

@executa_recusa_de_acordos
@solicita_aprovacao_proposta_a_prazo_01
Cenário: Solicitando aprovação da proposta a prazo
    Dado que eu faça a requisição para aprovação da proposta a prazo com o valor dez por cento menor que a dívida
    Então devo verificar se a requisição de solicitação de aprovação foi executada com sucesso

@verifica_status_proposta_a_prazo_01
Cenário: Verificando status da proposta a prazo
    Dado que eu faça a requisição para solicitação do status da proposta
    Então devo verificar se a requisição de verificação de status foi executada com sucesso
    E vou verificar se está com o status aprovado
    Então vou verificar se o status da proposta é semelhante ao encontrado no Soapui

@gerando_acordo_api_assessoria_a_prazo_02
Cenário: Gerando acordo no WS_COB com proposta a prazo
    Dado que eu faça a requisição para simulação de proposta a prazo com o valor vinte por cento menor que a dívida
    Então devo verificar se a requisição de geração de acordo foi executada com sucesso
    Quando eu realizar a requisição para simulação de proposta a prazo com o valor vinte por cento menor que a dívida no Soapui
    Então vou verificar se o valor total da dívida são semelhantes aos do Soapui
    E vou verificar se os valores máximos e mínimos por parcela são semelhantes aos do Soapui
    E verificar se os valores máximos e mínimos de entrada são semelhantes aos do Soapui
    E devo verificar se os valores máximos e mínimos dos honorários são semelhantes aos do Soapui

@executa_recusa_de_acordos
@solicita_aprovacao_proposta_a_prazo_02
Cenário: Solicitando aprovação da proposta a prazo
    Dado que eu faça a requisição para aprovação da proposta a prazo com o valor vinte por cento menor que a dívida
    Então devo verificar se a requisição de solicitação de aprovação foi executada com sucesso

@verifica_status_proposta_a_prazo_02
Cenário: Verificando status da proposta a prazo
    Dado que eu faça a requisição para solicitação do status da proposta
    Então devo verificar se a requisição de verificação de status foi executada com sucesso
    E vou verificar se está com o status aprovado
    Então vou verificar se o status da proposta é semelhante ao encontrado no Soapui

@gerando_acordo_api_assessoria_a_prazo_03
Cenário: Gerando acordo no WS_COB com proposta a prazo
    Dado que eu faça a requisição para simulação de proposta a prazo com o valor cinquenta por cento menor que a dívida
    Então devo verificar se a requisição de geração de acordo foi executada com sucesso
    Quando eu realizar a requisição para simulação de proposta a prazo com o valor cinquenta por cento menor que a dívida no Soapui
    Então vou verificar se o valor total da dívida são semelhantes aos do Soapui
    E vou verificar se os valores máximos e mínimos por parcela são semelhantes aos do Soapui
    E verificar se os valores máximos e mínimos de entrada são semelhantes aos do Soapui
    E devo verificar se os valores máximos e mínimos dos honorários são semelhantes aos do Soapui

@executa_recusa_de_acordos
@solicita_aprovacao_proposta_a_prazo_03
Cenário: Solicitando aprovação da proposta a prazo
    Dado que eu faça a requisição para aprovação da proposta a prazo com o valor cinquenta por cento menor que a dívida
    Então devo verificar se a requisição de solicitação de aprovação foi executada com sucesso

@verifica_status_proposta_a_prazo_03
Cenário: Verificando status da proposta a prazo
    Dado que eu faça a requisição para solicitação do status da proposta
    Então devo verificar se a requisição de verificação de status foi executada com sucesso
    E vou verificar se está com o status aprovado
    Então vou verificar se o status da proposta é semelhante ao encontrado no Soapui