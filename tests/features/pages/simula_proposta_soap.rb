class SimulaPropostaSoap < AutenticacaoSoap
    
    def exec_post_simula_proposta_soap(cliente, contrato, parcelas, forma_pagamento, parcela_valor, entrada)

        util = Util.new
        data = util.gera_data_hoje

        doc = exec_post_aut_soap() 
        idtoken = doc.at_xpath('//idToken').content
        validade_token = doc.at_xpath('//validadeToken').content
    
        url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        
        data = <<-EOF
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
            <soapenv:Header/>
                <soapenv:Body>
                    <ws:SimulaProposta>
                        <authentication>
                        <codigoParceiro>58</codigoParceiro>
                            <token>
                                <idToken>#{idtoken}</idToken>
                                <validadeToken>#{validade_token}</validadeToken>
                            </token>
                        </authentication>
                        <simulacao>
                            <usuario>2931MAURICIO</usuario>
                            <agente>2931</agente>
                            <contrato>#{contrato}</contrato>
                            <cliente>#{cliente}</cliente>
                            <parcelas>#{parcelas}</parcelas>
                            <data_pagamento>#{data}</data_pagamento>
                            <pagamento>
                                <forma>#{forma_pagamento}</forma>
                                <data>#{data}</data>
                                <entrada>
                                    <valor>#{entrada}</valor>
                                    <tarifa>0</tarifa>
                                    <honorario>0</honorario>
                                </entrada>
                                <parcela>
                                    <valor>#{parcela_valor}</valor>
                                    <honorario>0</honorario>
                                </parcela>
                            </pagamento>
                        </simulacao>
                    </ws:SimulaProposta>
                </soapenv:Body>
        </soapenv:Envelope>
    EOF
        
        headers = {
            'Content-Type' => 'text/xml; charset=utf-8',
            'Accept' => 'gzip,deflate',
            'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/simula_proposta"
        }
    
        result= http.post(uri.path, data, headers)
    
        doc = Nokogiri::XML(result.body)
        doc.remove_namespaces!
        simula_acordo = []
        valor_minimo = doc.at_xpath('//valor_min').content
        honorario_minimo = doc.at_xpath('//honorario').content
        honorario_max = doc.at_xpath('//honorario_max').content
        total_divida = doc.at_xpath('//total_divida').content
        parcelas_min = doc.at_xpath('//forma').content
        parcelas_max = doc.at_xpath('//forma_max').content

        simula_acordo << valor_minimo
        simula_acordo << honorario_minimo
        simula_acordo << honorario_max
        simula_acordo << total_divida
        simula_acordo << parcelas_min
        simula_acordo << parcelas_max

        return simula_acordo 
        end

        def exec_post_simula_proposta_soap_negativo(cliente, contrato, parcelas, forma_pagamento, parcela_valor, entrada)

            util = Util.new
            data = util.gera_data_hoje
    
            doc = exec_post_aut_soap() 
            idtoken = doc.at_xpath('//idToken').content
            validade_token = doc.at_xpath('//validadeToken').content
        
            url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
            uri = URI.parse(url)
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            
            data = <<-EOF
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
                <soapenv:Header/>
                    <soapenv:Body>
                        <ws:SimulaProposta>
                            <authentication>
                            <codigoParceiro>58</codigoParceiro>
                                <token>
                                    <idToken>#{idtoken}</idToken>
                                    <validadeToken>#{validade_token}</validadeToken>
                                </token>
                            </authentication>
                            <simulacao>
                                <usuario>2931MAURICIO</usuario>
                                <agente>2931</agente>
                                <contrato>#{contrato}</contrato>
                                <cliente>#{cliente}</cliente>
                                <parcelas>#{parcelas}</parcelas>
                                <data_pagamento>#{data}</data_pagamento>
                                <pagamento>
                                    <forma>#{forma_pagamento}</forma>
                                    <data>#{data}</data>
                                    <entrada>
                                        <valor>#{entrada}</valor>
                                        <tarifa>0</tarifa>
                                        <honorario>0</honorario>
                                    </entrada>
                                    <parcela>
                                        <valor>#{parcela_valor}</valor>
                                        <honorario>0</honorario>
                                    </parcela>
                                </pagamento>
                            </simulacao>
                        </ws:SimulaProposta>
                    </soapenv:Body>
            </soapenv:Envelope>
        EOF
            
            headers = {
                'Content-Type' => 'text/xml; charset=utf-8',
                'Accept' => 'gzip,deflate',
                'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/simula_proposta"
            }
        
            result= http.post(uri.path, data, headers)
        
            doc = Nokogiri::XML(result.body)
            doc.remove_namespaces!

            simula_acordo_soap_response=[]
            simula_acordo_soap_response << doc.at_xpath('//codigo').content
            simula_acordo_soap_response << doc.at_xpath('//descricao').content
            
    
            
            return simula_acordo_soap_response  
    
            end

            def exec_post_simula_proposta_soap_data(cliente, contrato, parcelas, forma_pagamento, parcela_valor, entrada, data_simulacao_soap)

                util = Util.new
                data = data_simulacao_soap
        
                doc = exec_post_aut_soap() 
                idtoken = doc.at_xpath('//idToken').content
                validade_token = doc.at_xpath('//validadeToken').content
            
                url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
                uri = URI.parse(url)
                http = Net::HTTP.new(uri.host, uri.port)
                http.use_ssl = true
                
                data = <<-EOF
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
                    <soapenv:Header/>
                        <soapenv:Body>
                            <ws:SimulaProposta>
                                <authentication>
                                <codigoParceiro>58</codigoParceiro>
                                    <token>
                                        <idToken>#{idtoken}</idToken>
                                        <validadeToken>#{validade_token}</validadeToken>
                                    </token>
                                </authentication>
                                <simulacao>
                                    <usuario>2931MAURICIO</usuario>
                                    <agente>2931</agente>
                                    <contrato>#{contrato}</contrato>
                                    <cliente>#{cliente}</cliente>
                                    <parcelas>#{parcelas}</parcelas>
                                    <data_pagamento>#{data}</data_pagamento>
                                    <pagamento>
                                        <forma>#{forma_pagamento}</forma>
                                        <data>#{data}</data>
                                        <entrada>
                                            <valor>#{entrada}</valor>
                                            <tarifa>0</tarifa>
                                            <honorario>0</honorario>
                                        </entrada>
                                        <parcela>
                                            <valor>#{parcela_valor}</valor>
                                            <honorario>0</honorario>
                                        </parcela>
                                    </pagamento>
                                </simulacao>
                            </ws:SimulaProposta>
                        </soapenv:Body>
                </soapenv:Envelope>
            EOF
                
                headers = {
                    'Content-Type' => 'text/xml; charset=utf-8',
                    'Accept' => 'gzip,deflate',
                    'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/simula_proposta"
                }
            
                result= http.post(uri.path, data, headers)
            
                doc = Nokogiri::XML(result.body)
                doc.remove_namespaces!
    
                simula_acordo_soap_response=[]
                simula_acordo_soap_response << doc.at_xpath('//codigo').content
                simula_acordo_soap_response << doc.at_xpath('//descricao').content
                
        
                
                return simula_acordo_soap_response  
        
                end
end