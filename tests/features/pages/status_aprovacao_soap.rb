class StatusAprovacaoSoap < AutenticacaoSoap

    def exec_post_status_aprovacao_soap(contrato, cliente)
    
        require_relative '../step_definitions/simula_acordo.rb'

        sequencia = $dados_solicita_aprovacao[2]

        doc = exec_post_aut_soap() 
        idtoken = doc.at_xpath('//idToken').content
        validade_token = doc.at_xpath('//validadeToken').content
    
        url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        
        data = <<-EOF
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
        <soapenv:Header/>
            <soapenv:Body>
                <ws:StatusAprovacao>
                    <authentication>
                        <codigoParceiro>58</codigoParceiro>
                    <token>
                        <idToken>#{idtoken}</idToken>
                        <validadeToken>#{validade_token}</validadeToken>
                    </token>
                    </authentication>
                    <status>
                        <contrato>#{contrato}</contrato>
                        <cliente>#{cliente}</cliente>
                        <sequencia>#{sequencia}</sequencia>
                    </status>
                </ws:StatusAprovacao>
            </soapenv:Body>
        </soapenv:Envelope>
        EOF
        
        headers = {
            'Content-Type' => 'text/xml; charset=utf-8',
            'Accept' => 'gzip,deflate',
            'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/status_aprovacao"
        }
    
        result= http.post(uri.path, data, headers)
    
        doc = Nokogiri::XML(result.body)
        doc.remove_namespaces!
        
        status_aprovacao = doc.at_xpath('//historico').content
        return status_aprovacao    
        end
    end