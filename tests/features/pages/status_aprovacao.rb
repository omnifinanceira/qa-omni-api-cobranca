class StatusAprovacao < Request

    def status_aprovacao(token, contrato, cliente)
        require_relative '../step_definitions/simula_acordo.rb'

        sequencia = $dados_solicita_aprovacao[2]

        status = exec_get("/acordos/contrato/#{contrato}/cliente/#{cliente}/sequencia/#{sequencia}/status-acordo", token)
        return status
    end

end