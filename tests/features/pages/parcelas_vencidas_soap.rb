class ParcelasVencidasSoap < AutenticacaoSoap

    def exec_post_parcelas_vencidas_soap(contrato, cliente, data_atual)
    
        util = Util.new
        dataAtual = util.gera_data_hoje

        doc = exec_post_aut_soap() 
        idtoken = doc.at_xpath('//idToken').content
        validade_token = doc.at_xpath('//validadeToken').content

    
        url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        
        data = <<-EOF
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
         <soapenv:Header/>
         <soapenv:Body>
            <ws:ParcelasVencidas>
                <authentication>
                <codigoParceiro>58</codigoParceiro>
                <token>
                    <idToken>#{idtoken}</idToken>
                    <validadeToken>#{validade_token}</validadeToken>
                </token>
                </authentication>
                    <venc>
                        <usuario>2931MAURICIO</usuario>
                        <contrato>#{contrato}</contrato>
                        <cliente>#{cliente}</cliente>
                        <data_pagamento>#{dataAtual}</data_pagamento>
                    </venc>
            </ws:ParcelasVencidas>
        </soapenv:Body>
        </soapenv:Envelope>

        EOF
        
        headers = {
            'Content-Type' => 'text/xml; charset=utf-8',
            'Accept' => 'gzip,deflate',
            'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/parcelas_vencidas"
        }
    
        result= http.post(uri.path, data, headers)
    
        doc = Nokogiri::XML(result.body)
        doc.remove_namespaces!

        countNodes = 0
        doc.traverse { |node| countNodes += 1 if node.name == "parcelas" }
       

        parcelas_vencidas_soap_response = []
        control =0
        while control < countNodes
            
            parcelas_vencidas_numero_SOAP = doc.xpath('//numero')
            parcelas_vencidas_valor_SOAP = doc.xpath('//valor')
            parcelas_vencidas_vencimento_SOAP = doc.xpath('//vencimento')
            parcelas_vencidas_principal_SOAP = doc.xpath('//principal')
            parcelas_vencidas_multa_SOAP = doc.xpath('//multa')
            parcelas_vencidas_mora_SOAP = doc.xpath('//mora')
            parcelas_vencidas_juros_SOAP = doc.xpath('//jurosRemuneratorios')
            parcelas_vencidas_despesas_SOAP = doc.xpath('//despesas')


            parcelas_vencidas_soap_response << parcelas_vencidas_numero_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_valor_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_vencimento_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_principal_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_multa_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_mora_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_juros_SOAP[control].content
            parcelas_vencidas_soap_response << parcelas_vencidas_despesas_SOAP[control].content

            control+=1
        end

        
        return parcelas_vencidas_soap_response    

        end




        def exec_post_parcelas_vencidas_soap_negativo(contrato, cliente, data_atual)
    
            util = Util.new
            dataAtual = data_atual
    
            doc = exec_post_aut_soap() 
            idtoken = doc.at_xpath('//idToken').content
            validade_token = doc.at_xpath('//validadeToken').content
    
        
            url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
            uri = URI.parse(url)
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            
            data = <<-EOF
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
             <soapenv:Header/>
             <soapenv:Body>
                <ws:ParcelasVencidas>
                    <authentication>
                    <codigoParceiro>58</codigoParceiro>
                    <token>
                        <idToken>#{idtoken}</idToken>
                        <validadeToken>#{validade_token}</validadeToken>
                    </token>
                    </authentication>
                        <venc>
                            <usuario>2931MAURICIO</usuario>
                            <contrato>#{contrato}</contrato>
                            <cliente>#{cliente}</cliente>
                            <data_pagamento>#{dataAtual}</data_pagamento>
                        </venc>
                </ws:ParcelasVencidas>
            </soapenv:Body>
            </soapenv:Envelope>
    
            EOF
            
            headers = {
                'Content-Type' => 'text/xml; charset=utf-8',
                'Accept' => 'gzip,deflate',
                'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/parcelas_vencidas"
            }
        
            result= http.post(uri.path, data, headers)
        
            doc = Nokogiri::XML(result.body)
            doc.remove_namespaces!
    

            parcelas_vencidas_soap_response=[]
            parcelas_vencidas_soap_response << doc.at_xpath('//codigo').content
            parcelas_vencidas_soap_response << doc.at_xpath('//descricao').content
            
    
            
            return parcelas_vencidas_soap_response    
    
            end
    end