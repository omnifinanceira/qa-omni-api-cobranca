class UrlBoleto < Request

    def solicita_url_boleto(token, contrato, cliente)
        require_relative '../step_definitions/simula_acordo.rb'

        sequencia = $dados_solicita_aprovacao[2]

        url_boleto = exec_post("/acordos/contrato/#{contrato}/cliente/#{cliente}/sequencia/#{sequencia}/gerar-boleto", token)
        return url_boleto
    end

end