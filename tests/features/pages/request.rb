require_relative '../pages/util.rb'

class Request < Util

    def initialize
        @url_padrao = CONFIG['url_geral']
    end

    def exec_post_aut(body)
        url_full = "https://dev-omni-auth.omni.com.br/auth/realms/omnifacil/protocol/openid-connect/token"

        response = HTTParty.post(url_full,
        :body => body,
        :headers => {'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/x-www-form-urlencoded' })

        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full, body, response)
        end

        return response
    end

    def exec_post(path, token)
        url_full = "#{@url_padrao}#{path}"

        response = HTTParty.post(url_full,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            "Authorization" => "Bearer #{token}"})

        unless response['Erros'].nil?
            gravar_request_response(url_full, body, response)
        end

        return response
    end

    def exec_post_body(path, body, token)
        url_full = "#{@url_padrao}#{path}"

        response = HTTParty.post(url_full,
        :body => body.to_json,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            "Authorization" => "Bearer #{token}"})

        unless response['Erros'].nil?
            gravar_request_response(url_full, body, response)
        end

        return response
    end

    def exec_get(path, token)
        url_full = "#{@url_padrao}#{path}"

        response = HTTParty.get(url_full,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            "Authorization" => "Bearer #{token}"})
        
        return response
    end 
end