class SolicitaAprovacao < Request

    def solicita_aprovacao(cliente, contrato, parcelas, forma_pagamento, parcela_valor, entrada, token)

        util = Util.new
        data = util.gera_data_hoje

        body = {
            "agente": "2931",
            "clienteId": "#{cliente}",
            "contrato": "#{contrato}",
            "formaPagamento": "#{forma_pagamento}",
            "honorarioAssessoriaEntrada": 0,
            "honorarioAssessoriaParcela": 0,
            "observacaoProposta": "Primeiro teste automatizado",
            "parcelas": "#{parcelas}",
            "percentualRemuneracao": 0.00,
            "remuneracaoCobranca": 0.00,
            "usuario": "2931MAURICIO",
            "valorBaseRemuneracao": 0.00,
            "valorEntrada": "#{entrada}",
            "valorParcela": "#{parcela_valor}",
            "vencimento": "#{data}"
            }

        solicitacao = exec_post_body("/acordos", body, token)
        return solicitacao
    end

end