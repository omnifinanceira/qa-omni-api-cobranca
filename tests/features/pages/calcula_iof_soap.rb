class CalculaIofSoap < AutenticacaoSoap

    def exec_post_calcula_iof_soap(contrato, parcelas)

    doc = exec_post_aut_soap() 
    idtoken = doc.at_xpath('//idToken').content
    validade_token = doc.at_xpath('//validadeToken').content
    
    url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
        
    data = <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
        <soapenv:Header/>
            <soapenv:Body>
                <ws:RetornaIof>
                    <authentication>
                    <codigoParceiro>58</codigoParceiro>
                        <token>
                            <idToken>#{idtoken}</idToken>
                            <validadeToken>#{validade_token}</validadeToken>
                        </token>
                    </authentication>
                        <iof>
                            <contrato>#{contrato}</contrato>
                            <parcelas>#{parcelas}</parcelas>
                            <data_pagamento>06/12/2021</data_pagamento>
                        </iof>
                </ws:RetornaIof>
            </soapenv:Body>
        </soapenv:Envelope>
    EOF
        
    headers = {
        'Content-Type' => 'text/xml; charset=utf-8',
        'Accept' => 'gzip,deflate',
        'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/retorna_iof"
    }
    result= http.post(uri.path, data, headers)
    doc = Nokogiri::XML(result.body)
    doc.remove_namespaces!
    
    calculo_iof = doc.at_xpath('//retretornaIof').content
    return calculo_iof    
    end
end