class SaldoAtualizadoSoap < AutenticacaoSoap

def exec_post_saldo_atualizado_soap(contrato, cliente)

    doc = exec_post_aut_soap() 
    idtoken = doc.at_xpath('//idToken').content
    validade_token = doc.at_xpath('//validadeToken').content

    url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    
    data = <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
        <soapenv:Header/>
            <soapenv:Body>
                <ws:SaldoAtualizado>
                    <authentication>
                    <codigoParceiro>58</codigoParceiro>
                    <token>
                    <idToken>#{idtoken}</idToken>
                    <validadeToken>#{validade_token}</validadeToken>
                    </token>
                    </authentication>
                    <saldo>
                    <contrato>#{contrato}</contrato>
                    <cliente>#{cliente}</cliente>
                    </saldo>
                </ws:SaldoAtualizado>
            </soapenv:Body>
        </soapenv:Envelope>
    EOF
    
    headers = {
        'Content-Type' => 'text/xml; charset=utf-8',
        'Accept' => 'gzip,deflate',
        'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/saldo_atualizado"
    }

    result= http.post(uri.path, data, headers)

    doc = Nokogiri::XML(result.body)
    doc.remove_namespaces!
    
    saldo_atualizado = doc.at_xpath('//retsaldoAtualizado').content
    return saldo_atualizado    
    end
end