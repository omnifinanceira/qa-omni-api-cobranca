class CalculaIof < Request

    def calcula_iof(token, contrato, parcelas)

        body = {
            "parcelas": "#{parcelas}",
            "dataPagamento": "06/12/2021"
        }

        iof = exec_post_body("/acordos/contrato/#{contrato}/calcula-iof", body, token)
        return iof
    end

end