class Autenticacao < Request

    def fazer_autenticacao(usuario = $usuario, senha = $senha)

        body = {
            "username": usuario,
            "password": senha,
            "client_secret": "9019aff9-32ce-4e1c-b3cf-6b952ecdac0d",
            "client_id": "assessoria",
            "grant_type": "password"
        }

        autenticacao = exec_post_aut(body)
        return autenticacao
    end

    def retorna_token 
        token = fazer_autenticacao['access_token']
        return token
    end

end