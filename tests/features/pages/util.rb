require_relative '../pages/geradorrandomico.rb'

class Util < GeradorRandomico

    def initialize
        $usuario = "2931MAURICIO"
        $senha = "senha123"
        criar_pasta_log
    end

    def pega_carteiras_assessoria()
        @sql = Queries.new
        lista_carteiras_assessoria = @sql.busca_carteiras_assessoria
        return lista_carteiras_assessoria.to_a
    end

    def gera_data_hoje
        ENV["TZ"]
        date = Time.now
        ENV["TZ"] = "America/Sao_Paulo"
        date = Time.now.strftime("%d/%m/%Y")
        return date
    end

    def criar_pasta_log
        @diretorio = "#{Dir.pwd}/reports"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end

    def gravar_request_response(path,request,response)
        arquivo = "#{Dir.pwd}/reports/retorno_api_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"

        arquivo = File.new(arquivo, "w")
        File.write(arquivo, "Servico: #{path}\n\n\nRequisição: #{request}\n\n\nRetorno: #{response}")
        arquivo.close
        
        arquivo2 = "#{Dir.pwd}/retorno_api_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo2 = File.new(arquivo2, "w")
        File.write(arquivo2, "Servico: #{path}\n\n\nRequisição: #{request}\n\n\nRetorno: #{response}")
        arquivo2.close
    end

    def gravar(request_return)
        arquivo = "#{Dir.pwd}/reports/#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo = File.new(arquivo, "w")
        File.write(arquivo, "#{request_return.to_json.force_encoding("UTF-8")}")
        arquivo.close
    end
end