class ParcelasVencidas < Request
    
    def busca_parcelas_vencidas(contrato,token,data_corrente)

        util = Util.new
        data = data_corrente

        body = {
            "contrato": "#{contrato}",
            "dataPagamento": "#{data}"
        }
        parcela_vencida_response = exec_post_body("/contratos/parcelas-vencidas",  body, token)

        return parcela_vencida_response

        
    end

   

end