class SimulaProposta < Request

    def simula_proposta(cliente, contrato, parcelas, forma_pagamento, parcela_valor, entrada, token)

        util = Util.new
        data = util.gera_data_hoje

        body = {
            "cliente": "#{cliente}",
            "contrato": "#{contrato}",
            "agente": "2931",
            "parcelas": "#{parcelas}",
            "pagamento":{
                "forma": "#{forma_pagamento}",
                "parcela":{ "valor": "#{parcela_valor}"},
                "entrada": {"valor": "#{entrada}"},
                "dataPrimeiroPagamento": "#{data}" 
            }
        }

        acordo = exec_post_body("/acordos/simular-acordo", body, token)
        return acordo
    end

    def simula_proposta_data(cliente, contrato, parcelas, forma_pagamento, parcela_valor, entrada, token, data_simulacao)

        util = Util.new
        data = data_simulacao

        body = {
            "cliente": "#{cliente}",
            "contrato": "#{contrato}",
            "agente": "2931",
            "parcelas": "#{parcelas}",
            "pagamento":{
                "forma": "#{forma_pagamento}",
                "parcela":{ "valor": "#{parcela_valor}"},
                "entrada": {"valor": "#{entrada}"},
                "dataPrimeiroPagamento": "#{data}" 
            }
        }

        acordo = exec_post_body("/acordos/simular-acordo", body, token)
        return acordo
    end

end