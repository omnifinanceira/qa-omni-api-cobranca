class AutenticacaoSoap

    def exec_post_aut_soap

    url = "https://hst4.omni.com.br/desenv/WS_COB.servidor_soap_cdc_cobranca.handle_request"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    data = <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://hst4.omni.com.br/ws_cob">
        <soapenv:Header/>
            <soapenv:Body>
                <ws:GetToken>
                    <cnpjParceiro>04472165000117</cnpjParceiro>
                    <codigoParceiro>58</codigoParceiro>
                    <versaoWS>1.0</versaoWS>
                </ws:GetToken>
            </soapenv:Body>
    </soapenv:Envelope>
    EOF

    headers = {
        'Content-Type' => 'text/xml; charset=utf-8',
        'Accept' => 'gzip,deflate',
        'SOAPAction' => "https://hst4.omni.com.br/ws_cob/ws_cdc_cobranca/get_token"
    }

    result= http.post(uri.path, data, headers)

    doc = Nokogiri::XML(result.body)
    doc.remove_namespaces!

    return doc
    end
end