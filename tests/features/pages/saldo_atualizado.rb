class SaldoAtualizado < Request

    def busca_saldo(token, contrato, cliente)
        saldo = exec_get("/contracts/contrato/#{contrato}/cliente/#{cliente}/saldo-atualizado", token)
        return saldo
    end

end